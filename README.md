# 2log Dot Case

The case can be printed with normal PLA. I particularly like the matte look of the[ Extrudr NX2 black matte](https://www.amazon.de/-/en/extrudr®-PLA-3D-Printer-Filament/dp/B0741CGGDG/ref=sr_1_1?crid=3E6D7HWEK8ASD&keywords=extrudr+nx2+schwarz+matt&qid=1646768123&sprefix=extrudr+nx2+schwarz+matt%2Caps%2C66&sr=8-1). For the lid, simply laser a circle with a diameter of 70.5 mm. The material used for this is [BLACK & WHITE 9H04 SC](https://www.plexiglas-shop.com/en-de/products/plexiglas-led/pl9h04-sc-3-00-3050x2030-b-01-s.html?listtype=search&searchparam=PLEXIGLAS®%20LED%2C%20Black%20%26%20White%209H04%20SC).


